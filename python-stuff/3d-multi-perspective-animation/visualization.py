import numpy as np
from numpy import sin, cos, pi
from scipy import interpolate
import vtk
from vtk.util.colors import *
from addict import Addict


vtk.vtkObject.GlobalWarningDisplayOff()


# rotational matrices (probably with non-canonical sign convention)


def Rx(phi):
    return np.array([[1, 0, 0], [0, cos(phi), sin(phi)], [0, -sin(phi), cos(phi)]])


def Ry(phi):
    return np.array([[cos(phi), 0, -sin(phi)], [0, 1, 0], [sin(phi), 0, cos(phi)]])


def Rz(phi):
    return np.array([[cos(phi), sin(phi), 0], [-sin(phi), cos(phi), 0], [0, 0, 1]])


# create functions time dependent rotation angles functions
tt = np.linspace(0, 18, 10000)
dt = tt[1] - tt[0]

xx0 = sin(tt * 2 * pi)
xx1 = xx0**2 * (xx0 > 0)
xx2 = np.cumsum(xx1) * dt
xx3 = sin(xx2 * 2 * pi)
xx4 = xx3**2 * (xx3 > 0)
xx5 = np.cumsum(xx4) * dt / 4

if 0:
    # debugging visualization for timing functions
    import matplotlib.pyplot as plt

    plt.figure(figsize=(18, 10))
    plt.plot(tt, xx0)
    plt.plot(tt, xx1, label="$x_1$")
    plt.plot(tt, xx2, label="$x_2$")
    plt.plot(tt, xx3, label="$x_3$")
    plt.plot(tt, xx4, label="$x_4$")
    plt.plot(tt, xx5, label="$x_5$")

    plt.legend(loc="upper left")
    plt.grid()
    plt.xticks(np.arange(-1, 9, 0.25))
    plt.show()
    exit()

xx5f = interpolate.interp1d(tt, xx5, bounds_error=False, fill_value=(0, float("nan")))


def setPokeMatrix(actor, r, R):
    """
    The position of 3D objects in vtk is defined with a so-called poke matrix.
    It is a 4x4 matrix with the following shape:

    [          ]
    [   R     r]
    [          ]
    [0  0  0  1]

    If you want to update the position and orientation of a body, the values of the
    poke matrix must be changed. This is done with the help of this function.

    :param actor:   vtk Actor instance
    :param r:       array with shape (3,) – displacement vector
    :param R:       array with shape (3, 3) – orientation matrix
    """

    # create empty vtk matrix (actually is a 4x4 unit matrix)
    # its elements will be overwritten soon
    poke = vtk.vtkMatrix4x4()

    # stack together the received arguments of the functions
    P = np.column_stack((R, r))
    P = np.row_stack((P, np.array([0, 0, 0, 1])))

    # P is now a numpy array with shape (4, 4); its elements are copied to the above vtk matrix
    vtk.vtkMatrix4x4.DeepCopy(poke, P.flatten())

    # pass the poke matrix to the actor
    actor.PokeMatrix(poke)


# load geometry from stl file
GEB_object_source = vtk.vtkSTLReader()

# source of 3D-object: https://www.thingiverse.com/thing:25755/files
GEB_object_source.SetFileName("GEB.stl")

GEB_object_mapper = vtk.vtkPolyDataMapper()
GEB_object_mapper.SetInputConnection(GEB_object_source.GetOutputPort())

GEB_object_actor = vtk.vtkLODActor()
GEB_object_actor.SetMapper(GEB_object_mapper)

# initial orientation
setPokeMatrix(GEB_object_actor, [0, 0, 0], Ry(0.5 * pi))


camera = vtk.vtkCamera()
camera.SetPosition(0, 0, 30)
camera.SetFocalPoint(0, 0, -0)


# create Renderer, RenderWindow and RenderWindowInteractor
ren = vtk.vtkRenderer()
ren.SetActiveCamera(camera)
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# add actors to renderer
ren.AddActor(GEB_object_actor)

# background color and window size
ren.SetBackground(0.1, 0.2, 0.4)
renWin.SetSize(1000, 1000)

# select a more convenient interactor style and initialize
iren.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
iren.Initialize()


# step time in s
T_step = 20e-3  # 20ms
T_period = 0.5


class Animation:
    def __init__(self):
        self.step_counter = 0
        steps_per_period = T_period / T_step

        self.freq = 1 / steps_per_period

    def updateScene(self, *args):

        alpha = (self.step_counter * self.freq) % tt[-1]

        if alpha % 1 == 0:
            print(alpha)

        r_GEB_object = np.array([0, 0, 0])

        phi_1 = xx5f(alpha - 2) * 2 * pi
        phi_2 = xx5f(alpha - 4) * 2 * pi
        phi_3 = xx5f(alpha - 6) * 2 * pi
        R_GEB_object = Rx(phi_2) @ Ry(-phi_1)

        setPokeMatrix(GEB_object_actor, r_GEB_object, R_GEB_object @ Ry(0.5 * pi))

        # re-render the image
        renWin.Render()
        self.step_counter += 1


ani = Animation()

# Create the timer. The updateScene function will be called every 20ms automatically
iren.AddObserver("TimerEvent", ani.updateScene)
iren.CreateRepeatingTimer(int(T_step * 1e3))

# open the window
iren.Start()

# shut down after window was closed (by mouse or by "q" key)
iren.GetRenderWindow().Finalize()
